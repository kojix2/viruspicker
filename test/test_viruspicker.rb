# frozen_string_literal: true

require_relative 'test_helper'

class VirusPickerTest < Test::Unit::TestCase
  def test_that_it_has_a_version_number
    assert_kind_of String, ::VirusPicker::VERSION
  end
end

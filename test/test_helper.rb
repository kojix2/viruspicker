# frozen_string_literal: true

require 'simplecov'
SimpleCov.start

require 'virus_picker'

require 'test/unit'

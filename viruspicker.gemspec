# frozen_string_literal: true

require_relative 'lib/virus_picker/version'

Gem::Specification.new do |spec|
  spec.name          = 'viruspicker'
  spec.version       = VirusPicker::VERSION
  spec.authors       = ['kojix2']
  spec.email         = ['2xijok@gmail.com']

  spec.summary       = 'Detecting virus integration sites.'
  spec.description   = 'Detecting virus integration sites.'
  spec.homepage      = 'https://github.com/kojix2/viruspicker'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.4.0')

  spec.files         = Dir['*.{md,txt}', '{lib,exe}/**/*']
  spec.bindir        = 'exe'
  spec.executables   = ['viruspicker']
  spec.require_paths = ['lib']

  spec.add_dependency 'miniflow'

  spec.add_dependency 'tty-box'
  spec.add_dependency 'tty-command'
  spec.add_dependency 'tty-screen'
  spec.add_dependency 'tty-tree'

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'test-unit'
  spec.add_development_dependency 'yard'
end

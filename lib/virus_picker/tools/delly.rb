# frozen_string_literal: true

require_relative 'base_tool'

module VirusPicker
  module Tools
    # DELLY2: Structural variant discovery by integrated paired-end and split-read analysis
    # https://github.com/dellytools/delly
    class Delly < BaseTool
      def initialize(command: 'delly')
        @command = command
        available?
      end

      def available?
        super("#{@command} --version")
      end

      # Since call is a Ruby method, we need to overwrite it and execute the
      # call subcommand of delly.
      def call(*args)
        cmd.run2(@command, 'call', *args)
      end
    end
  end
end

# frozen_string_literal: true

require_relative 'base_tool'

module VirusPicker
  module Tools
    # bcftools - utilities for variant calling and manipulating VCFs and BCFs.
    # https://github.com/samtools/bcftools
    class Bcftools < BaseTool
      def initialize(command: 'bcftools')
        @command = command
        available?
      end

      def available?
        super("#{@command} --version")
      end

      # Since call is a Ruby method, we need to overwrite it and execute the
      # call subcommand of bcftool.
      def call(*args)
        cmd.run2(@command, 'call', *args)
      end
    end
  end
end

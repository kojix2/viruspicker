# frozen_string_literal: true

require_relative 'base_tool'

module VirusPicker
  module Tools
    # Burrow-Wheeler Aligner for short-read alignment.
    # https://github.com/lh3/bwa
    class BWA < BaseTool
      attr_accessor :rg, :verbosity

      def initialize(command: 'bwa', verbosity: nil, read_group: nil)
        @command = command
        @options = {}
        @options[:RG] = read_group ||
                        '"@RG\tID:VIRUSPICKER\tSM:VIRUSPICKER\tPL:illumina\tLB:VIRUSPICKER"'
        # verbosity level: 1=error, 2=warning, 3=message, 4+=debugging
        @options[:verbosity] = verbosity || 1
        available?
      end

      def available?
        super("which #{@command}")
      end
    end
  end
end

# frozen_string_literal: true

require_relative 'base_tool'

module VirusPicker
  module Tools
    # A versatile pairwise aligner for genomic and spliced nucleotide sequences
    # https://github.com/lh3/minimap2
    class Minimap2 < BaseTool
      attr_accessor :rg, :verbosity

      def initialize(command: 'minimap2', read_group: nil)
        @command = command
        @options = {}
        @options[:RG] = read_group ||
                        '"@RG\tID:VIRUSPICKER\tSM:VIRUSPICKER\tPL:illumina\tLB:VIRUSPICKER"'
        available?
      end

      def available?
        super("#{@command} --version")
      end
    end
  end
end

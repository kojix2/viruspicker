# frozen_string_literal: true

require 'tty-box'
require 'tty-screen'

module VirusPicker
  module Tools
    # Burrow-Wheeler Aligner for short-read alignment.
    class BaseTool < Miniflow::Tool
    end
  end
end

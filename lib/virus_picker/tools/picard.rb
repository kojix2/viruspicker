# frozen_string_literal: true

require_relative 'base_tool'

module VirusPicker
  module Tools
    # A set of command line tools (in Java) for manipulating high-throughput
    # sequencing (HTS) data and formats such as SAM/BAM/CRAM and VCF.
    # https://github.com/broadinstitute/picard
    class Picard < BaseTool
      def initialize(command: 'picard')
        @command = command
        available?
      end

      def available?
        super("#{@command} ViewSam --version")
      end
    end
  end
end

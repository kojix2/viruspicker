# frozen_string_literal: true

require_relative 'base_tool'

module VirusPicker
  module Tools
    # bedtools: a powerful toolset for genome arithmetic
    # https://github.com/arq5x/bedtools2
    class Bedtools < BaseTool
      def initialize(command: 'bedtools')
        @command = command
        available?
      end

      def available?
        super("#{@command} --version")
      end
    end
  end
end

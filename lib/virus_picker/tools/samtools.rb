# frozen_string_literal: true

require_relative 'base_tool'

module VirusPicker
  module Tools
    # Utilities for the Sequence Alignment/Map (SAM) format.
    # https://github.com/samtools/samtools
    class Samtools < BaseTool
      def initialize(threads: 1, tmp_dir: nil, command: 'samtools')
        @command = command
        available?
        @threads = threads
        @tmp_dir = tmp_dir
      end

      def available?
        super("#{@command} --version")
      end

      # Execute the `samtools sort` command.
      # Outputs `name.sort.bam` in the same directory as the input bam file.
      # Remove sam or unsorted bam file.
      # @param in_bam [String]
      def sort!(in_path)
        check_file(in_path, ['.bam', '.sam'])
        dirname = File.dirname(in_path)
        basename = File.basename(in_path, File.extname(in_path))
        sorted_bam_path = File.join(dirname, "#{basename}.sort.bam")
        if @tmp_dir
          cmd.run @command, 'sort',
                  '-@', @threads,
                  '-T', @tmp_dir,
                  '-o', sorted_bam_path,
                  in_path
        else
          cmd.run @command, 'sort',
                  '-@', @threads,
                  '-o', sorted_bam_path,
                  in_path
        end
        # Delete the Sam file because the file size is too large.
        cmd.run "rm #{in_path}"
      end

      # Index a coordinate-sorted BAM file.
      # Note: do not support CRAM file.
      def index!(in_bam)
        check_file(in_bam, '.bam')
        cmd.run @command, 'index',
                '-@', @threads,
                in_bam
      end
    end
  end
end

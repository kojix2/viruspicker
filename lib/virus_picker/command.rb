# frozen_string_literal: true

# VirusPicker: https://github.com/kojix2/viruspicker
# Author: kojix2 <2xijok@gmail.com>

require 'optparse'
require_relative 'command/parser'
require_relative 'flows'

module VirusPicker
  # The command line interface for Virus Picker.
  class Command
    attr_reader :parser

    def initialize
      @parser = Parser.new
    end

    def run
      argv = parser.parse_options
      params = parser.params&.compact || {}

      case parser.command

      when :call
        flow_class = Flows::Call

      when :doctor
        flow_class = VirusPicker::Flows::Doctor

      # extract unmapped reads
      when :filter
        params[:out_dir] ||= 'viruspicker/filter'
        flow_class = Flows::Filter

      # select the best virus sequence from the database
      when :select
        params[:out_dir] ||= 'viruspicker/select'
        flow_class = Flows::Select

      # make virus consensus sequence
      when :consensus
        params[:out_dir] ||= 'viruspicker/consensus'
        flow_class = Flows::Consensus

      when :predetect
        params[:out_dir] ||= 'viruspicker/predetect'
        flow_class = Flows::PreDetect

      when :host
        params[:out_dir] ||= 'viruspicker/host'
        flow_class = Flows::Host

      when :detect
        params[:out_dir] ||= 'viruspicker/detect'
        flow_class = Flows::Detect

      else
        raise 'Unknown command'
      end

      puts
      puts TTY::Box.frame(
        '  Github: https://github.com/kojix2/viruspicker',
        '  Contact: kojix2 <2xijok@gmail.com>',
        '  ',
        "  Command: #{parser.command}",
        '  ARGV:',
        *ARGV.map { |i| "    - #{i}" },
        '  Params:',
        *params&.map { |i| "    - #{i}" },
        title: { top_left: " Virus Picker #{VirusPicker::VERSION} " },
        width: TTY::Screen.width
      )

      flow_class.new(*argv, **params).run
    end
  end
end

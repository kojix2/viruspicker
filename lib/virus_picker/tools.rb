# frozen_string_literal: true

# VirusPicker: https://github.com/kojix2/viruspicker
# Author: kojix2 <2xijok@gmail.com>

require_relative 'tools/base_tool'

require_relative 'tools/samtools'
require_relative 'tools/bwa'
require_relative 'tools/minimap2'
require_relative 'tools/bcftools'
require_relative 'tools/bedtools'
require_relative 'tools/delly'
# require_relative 'tools/picard'

module VirusPicker
  # Command Line Interface
  module Tools
  end
end

# frozen_string_literal: true

# VirusPicker: https://github.com/kojix2/viruspicker
# Author: kojix2 <2xijok@gmail.com>

require 'optparse'

module VirusPicker
  # The command line interface for Virus Picker.
  class Command
    # Parsing command line options
    class Parser
      attr_reader :command, :params, :args
      attr_accessor :arg_keys

      def initialize
        @command  = nil
        @params   = {}
        @arg_keys = []
      end

      def create_parser
        OptionParser.new do |opt|
          opt.program_name = 'viruspicker'
          opt.version = VirusPicker::VERSION
          yield opt if block_given?
        end
      end

      def main_parser
        return @main_parser if @main_parser

        parser = create_parser
        # Usage and help messages
        parser.banner = \
          <<~MSG

            Program: viruspicker
            Version: #{VirusPicker::VERSION}
            Contact: kojix2 <2xijok@gmail.com>

            Usage:   viruspicker <command> [options]

            Command:
                call         run all
                doctor       check the dependent tools are available

            --- sub processes

             1  filter       extract unmapped reads from a bam file
             2  select       select the closest viral sequence
             3  consensus    make the consensus sequence of virus
             4  predetect    predict the integration sites with pair end reads
             5  host         make the consensus sequence of host.
             6  detect       adetect virus integration sites more accurately

          MSG
        @main_parser = parser
      end

      def sub_parser
        return @sub_parser if @sub_parser

        parser = create_parser

        case command
        when nil
          warn main_parser.help
          exit 1

        when :call
          arg_keys = ['in.bam', 'ref.fa', 'v-seqs.fa']
          params = { threads: 1,
                     tmp_dir: nil }
          parser.on('-t', '--threads INT', Integer, 'number of threads[1]') do |v|
            params[:threads] = v
          end
          parser.on('-o DIR', String, 'directory to output results to') do |v|
            params[:output_directory] = v
          end

        when :doctor
          arg_keys = []

        when :filter
          arg_keys = ['in.bam']
          params = { threads: 1,
                     tmp_dir: nil }
          parser.on('-t', '--threads INT', Integer, 'number of threads[1]') do |v|
            params[:threads] = v
          end
          parser.on('-o DIR', String, 'directory to output results to') do |v|
            params[:out_dir] = v
          end

        when :select
          arg_keys = ['unmapp.bam', 'viruses.fa']
          params = { threads: 1,
                     tmp_dir: nil }
          parser.on('-t', '--threads INT', Integer, 'number of threads[1]') do |v|
            params[:threads] = v
          end
          parser.on('-o DIR', String, 'directory to output results to') do |v|
            params[:out_dir] = v
          end

        when :consensus
          arg_keys = ['in.bam', 'virus.fa']
          params = { threads: 1,
                     tmp_dir: nil }
          parser.on('-t', '--threads INT', Integer, 'number of threads[1]') do |v|
            params[:threads] = v
          end
          parser.on('-o DIR', String, 'directory to output results to') do |v|
            params[:out_dir] = v
          end

        when :predetect
          arg_keys = ['in.bam', 'h_ref.fa', 'v_con.fa']
          params = { threads: 1,
                     tmp_dir: nil }
          parser.on('-t', '--threads INT', Integer, 'number of threads[1]') do |v|
            params[:threads] = v
          end
          parser.on('-o DIR', String, 'directory to output results to') do |v|
            params[:out_dir] = v
          end

        when :host
          arg_keys = ['h_ref.fa', 'clusters.bed', 'in.bam'] # FIXME
          params = { threads: 1,
                     tmp_dir: nil }
          parser.on('-t', '--threads INT', Integer, 'number of threads[1]') do |v|
            params[:threads] = v
          end
          parser.on('-o DIR', String, 'directory to output results to') do |v|
            params[:out_dir] = v
          end

        when :detect
          arg_keys = ['all.bam', 'h_ref.fa', 'v_ref.fa']
          params = { threads: 1,
                     tmp_dir: nil }
          parser.on('-t', '--threads INT', Integer, 'number of threads[1]') do |v|
            params[:threads] = v
          end
          parser.on('-o DIR', String, 'directory to output results to') do |v|
            params[:out_dir] = v
          end

        else
          warn "viruspicker: unrecognized command '#{command}'"
          exit 1
        end

        parser.separator('')

        parser.banner = <<~MSG

          Usage: viruspicker #{command} [options] #{arg_keys.map { |i| "<#{i}>" }.join(' ')}

          Options:
        MSG

        @arg_keys = arg_keys
        @params = params
        @sub_parser = parser
      end

      def parse_options(argv = ARGV)
        begin
          main_parser.order!(argv)
        rescue OptionParser::ParseError => e
          warn "viruspicker: #{e.message}"
          exit 1
        end

        @command = argv.shift&.to_sym

        if argv.empty? && command != :doctor # FIXME
          warn sub_parser.help
          exit 1
        end

        begin
          sub_parser.parse!(argv)
        rescue OptionParser::ParseError => e
          warn "viruspicker: #{e.message}"
          exit 1
        end

        @args = argv_take(@arg_keys.size)
      end

      private

      # Check ARGV size.
      def argv_take(n)
        if ARGV.size != n
          msg = "[#{command}]" \
                ' wrong number of command-line arguments ' \
                "(given #{ARGV.size}, expected #{n})"
          warn Pastel.new.red(msg)
          exit 1
        end
        ARGV.take(n)
      end
    end
  end
end

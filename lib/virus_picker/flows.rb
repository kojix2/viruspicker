# frozen_string_literal: true

# VirusPicker: https://github.com/kojix2/viruspicker
# Author: kojix2 <2xijok@gmail.com>

require_relative 'flows/base_flow'

require_relative 'flows/0_call'
require_relative 'flows/1_filter'
require_relative 'flows/2_select'
require_relative 'flows/3_consensus'
require_relative 'flows/4_pre_detect'
require_relative 'flows/5_host'
require_relative 'flows/6_detect'

require_relative 'flows/doctor'

module VirusPicker
  # This module contains flows
  module Flows
  end
end

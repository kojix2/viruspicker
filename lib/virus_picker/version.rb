# frozen_string_literal: true

# VirusPicker: https://github.com/kojix2/viruspicker
# Author: kojix2 <2xijok@gmail.com>

# Virus Picker is a tool to detect virus integration site.
module VirusPicker
  VERSION = '0.0.1'
end

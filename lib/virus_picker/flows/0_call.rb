# frozen_string_literal: true

# VirusPicker: https://github.com/kojix2/viruspicker
# Author: kojix2 <2xijok@gmail.com>

require 'fileutils'
require_relative 'base_flow'

module VirusPicker
  module Flows
    # Run all workflows in order.
    class Call < BaseFlow
      # @param bam_path [String]
      # @param human_reference_path [String]
      # @param virus_reference_path [String]
      # @param params
      def initialize(bam_path,
                     human_reference_path,
                     virus_sequences_path,
                     **params)
        @bam_path             = File.expand_path(bam_path)
        @human_reference_path = File.expand_path(human_reference_path)
        @virus_sequences_path = File.expand_path(virus_sequences_path)
        @output_directory     = File.expand_path(
          params[:output_directory] || 'viruspicker'
        )
        params.delete(:output_directory)
        @params = params
      end

      def before_run
        # Check if tools are available
        doctor = Doctor.new
        doctor.run
        if doctor.error?
          show_exit_error
          exit 1
        end
        unless Dir.exist? @output_directory
          puts "create output directory: #{@output_directory}"
          mkdir_p @output_directory
        end
      end

      def after_run
        show_results(generated_files: false)
      end

      def main_run
        # List of directories
        filter_dir    = File.join(@output_directory, '1_filter')
        select_dir    = File.join(@output_directory, '2_select')
        consensus_dir = File.join(@output_directory, '3_consensus')
        predetect_dir = File.join(@output_directory, '4_predetect')
        host_dir      = File.join(@output_directory, '5_host')
        detect_dir    = File.join(@output_directory, '6_detect')

        # 1_filter
        @params[:out_dir] = filter_dir
        Filter.new(@bam_path, **@params).run

        # 2_select
        @params[:out_dir] = select_dir
        Select.new(File.join(filter_dir, 'human_unmapped_all.human.bam'),
                   @virus_sequences_path,
                   **@params).run

        # 3_consensus
        @params[:out_dir] = consensus_dir
        Consensus.new(File.join(select_dir, 'human_unmapped_all.selected_virus.sort.bam'),
                      File.join(select_dir, 'selected_virus.fa'),
                      **@params).run

        # 4_predetect
        @params[:out_dir] = predetect_dir
        candidate_found_flag = PreDetect.new(
          File.join(filter_dir, 'human_one_end_mapped.human.bam'),
          @human_reference_path,
          File.join(consensus_dir, 'virus_consensus.fa'),
          **@params
        ).run

        # Exit if candidate reads not found
        if candidate_found_flag == false
          puts 'skip 5_host'
          puts 'skip 6_detect'
          return
        end

        # 5_host
        @params[:out_dir] = host_dir
        Host.new(@human_reference_path,
                 File.join(predetect_dir, 'candidate_regions.bed'),
                 @bam_path,
                 **@params).run

        # 6_detect
        @params[:out_dir] = detect_dir
        Detect.new(@bam_path,
                   File.join(host_dir, 'host_consensus.fa'),
                   File.join(consensus_dir, 'virus_consensus.fa'),
                   **@params).run
      end
    end
  end
end

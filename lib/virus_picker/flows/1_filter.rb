# frozen_string_literal: true

# VirusPicker: https://github.com/kojix2/viruspicker
# Author: kojix2 <2xijok@gmail.com>

require_relative 'base_flow'

module VirusPicker
  module Flows
    # Extract the reads that were mapped to the human genome.
    class Filter < BaseFlow
      # @param bam_path [String] Path of the bam file to be analyzed
      # @param out_dir [String] Specify the output directory of the files
      # @param tmp_dir [String] Specify where Samtools writes temporary files
      # @param threads [Integer] The number of threads used in Samtools
      def initialize(bam_path,
                     out_dir:,
                     tmp_dir: nil,
                     threads: 1)

        @bam_path = File.expand_path(bam_path)

        @out_dir  = File.expand_path(out_dir)
        @tmp_dir  = File.expand_path(tmp_dir) if tmp_dir

        @threads = threads
      end

      def before_run
        @samtools = Tools::Samtools.new(threads: @threads, tmp_dir: @tmp_dir)
        check_file(@bam_path, '.bam')
        result = samtools.view '-H',
                               @bam_path,
                               '| grep "@HD"'
        unless result.out.include?('SO:coordinate')
          warn 'bam file not sorted.'
          raise TTY::Command::ExitError
        end
        mkdir_p @out_dir
      end

      def after_run
        check_output_files
        show_results
      end

      def main_run
        base_dir   = File.dirname(@bam_path)
        base_name  = File.basename(@bam_path, '.bam')

        # Indexing
        samtools.index! @bam_path

        # Extract unmapped reads and partially mapped reads.
        # Meaning of the flags
        # 0x4     4       UNMAP
        # 0x8     8       MUNMAP
        # 0xc     12      UNMAP,MUNMAP
        # 0x104   260     UNMAP,SECONDARY
        # 0x108   264     MUNMAP,SECONDARY

        # mate unmapped && read mapped && primary alignment
        # 0x8     8       MUNMAP
        # 0x104   260     UNMAP,SECONDARY
        samtools.view '-f', 8,    # only include reads with all of the FLAGs
                      '-F', 260,  # only include reads with none of the FLAGs
                      '-u',       # uncompressed BAM output
                      '-b',       # output BAM
                      '-@', @threads,
                      '-o', odir('human_read_mapped_mate_unmapped.human.bam'),
                      @bam_path

        # read unmapped && mate mapped && primary alignment
        # 0x4     4       UNMAP
        # 0x108   264     MUNMAP,SECONDARY
        samtools.view '-f', 4,
                      '-F', 264,
                      '-u',
                      '-b',
                      '-@', @threads,
                      '-o', odir('human_read_unmapped_mate_mapped.human.bam'),
                      @bam_path

        # read unmapped && mate unmapped
        # 0xc     12      UNMAP,MUNMAP
        samtools.view '-f', 12,
                      '-u',
                      '-b',
                      '-@', @threads,
                      '-o', odir('human_unmapped.human.bam'),
                      @bam_path

        # Merge all unmapped reads.
        # Ummapped pair or one end mapped pair reads.
        samtools.merge '-u',
                       '-@', @threads,
                       '-f', # Overwrite the output BAM if exist FIXME
                       odir('human_unmapped_all.human.bam'),
                       dir('human_read_mapped_mate_unmapped.human.bam'),
                       dir('human_read_unmapped_mate_mapped.human.bam'),
                       dir('human_unmapped.human.bam')

        # Merge one end mapped reads.
        samtools.merge '-u',
                       '-@', @threads,
                       '-f', # Overwrite the output BAM if exist FIXME
                       odir('human_one_end_mapped.human.bam'),
                       dir('human_read_mapped_mate_unmapped.human.bam'),
                       dir('human_read_unmapped_mate_mapped.human.bam')
      end
    end
  end
end

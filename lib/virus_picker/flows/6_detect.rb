# frozen_string_literal: true

# VirusPicker: https://github.com/kojix2/viruspicker
# Author: kojix2 <2xijok@gmail.com>

require_relative 'base_flow'

module VirusPicker
  module Flows
    # @param bam_path [String]
    # @param human_reference_path [String]
    # @param virus_reference_path [String]
    # @param out_dir [String] Specify the output directory of the files
    # @param tmp_dir [String] Specify where Samtools writes temporary files
    # @param read_group [String]
    # @param threads [Integer] The number of threads used in Samtools
    class Detect < BaseFlow
      def initialize(bam_path,
                     human_reference_path,
                     virus_reference_path,
                     out_dir:,
                     tmp_dir:    nil,
                     read_group: nil,
                     threads:    nil)

        @bam_path             = File.expand_path(bam_path)
        @human_reference_path = File.expand_path(human_reference_path)
        @virus_reference_path = File.expand_path(virus_reference_path)

        @out_dir              = File.expand_path(out_dir)
        @tmp_dir              = File.expand_path(tmp_dir) if tmp_dir

        @read_group           = read_group
        @threads              = threads || 1
      end

      def before_run
        @bwa      = Tools::BWA.new(read_group: @read_group)
        @samtools = Tools::Samtools.new(threads: @threads, tmp_dir: @tmp_dir)
        @delly    = Tools::Delly.new
        mkdir_p @out_dir
      end

      def after_run
        check_output_files
        show_results
      end

      # Get Fasta Strategy
      # fixed.fa
      # bedtools.getfasta '-fi', @human_reference_path,
      #                   '-bed', dir('candidates_regions_extended.bed'),
      #                   '-name',
      #                   '>', odir('fixed.fa') # FIXM

      def main_run
        # Merging the Human Genome and the Consensus Viral Genome.
        cmd.run "cat #{@human_reference_path} #{@virus_reference_path}" \
                "> #{odir('human_virus.fa')}"

        # Capitalize the reference genome (Viral genomes are often lower case)
        lines = File.readlines(dir('human_virus.fa')).map do |line|
          # Chromosome names and line breaks intact
          line.start_with?('<') ? line : line.upcase
        end
        File.write(odir('human_virus.fa'), lines.join)

        # Make index
        # FIXME this process may take a lot of time.
        # Get fasta strategy may be better if possible.

        bwa.index odir('human_virus.fa')

        # Shuffle and group alignments by name.
        samtools.collate '-@', @threads,
                         '-o', odir('all.collate.bam'),
                         @bam_path

        # Extract the fastq file from the bam file.
        samtools.fastq '-N',
                       '-@', @threads,
                       '-o', odir('all.fastq'),
                       dir('all.collate.bam')

        bwa.mem '-p', # smart pairing (ignoring in2.fq)
                '-R', bwa[:RG],
                '-t', @threads,
                '-v', bwa[:verbosity],
                '-o', odir('all.sam'),
                dir('human_virus.fa'),
                dir('all.fastq')

        # Sorting
        samtools.sort! dir('all.sam')

        # Indexing
        samtools.index! dir('all.sort.bam')

        # delly
        delly.call '-o', odir('delly.bcf'),
                   '-g', dir('human_virus.fa'),
                   dir('all.sort.bam')
      end
    end
  end
end

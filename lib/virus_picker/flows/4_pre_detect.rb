# frozen_string_literal: true

# VirusPicker: https://github.com/kojix2/viruspicker
# Author: kojix2 <2xijok@gmail.com>

require_relative 'base_flow'

module VirusPicker
  module Flows
    # Extract the candidate parts of the reference genome sequence.
    class PreDetect < BaseFlow
      # @param bam_path [String] Discordant reads. `one_end_mapped.human.collate.bam`
      # @param human_reference_path [String]
      # @param virus_consensus_path [String]
      # @param out_dir [String] Specify the output directory of the files
      # @param tmp_dir [String] Specify where Samtools writes temporary files
      # @param read_group [String]
      # @param threads [Integer] The number of threads used in BWA
      def initialize(human_one_end_mapped_bam_path,
                     human_reference_path,
                     virus_consensus_path,
                     out_dir:,
                     tmp_dir:              nil,
                     read_group:           nil,
                     threads:              nil,
                     max_cluster_distance: nil)

        @human_one_end_mapped_bam = File.expand_path(human_one_end_mapped_bam_path)
        @human_reference          = File.expand_path(human_reference_path)
        @virus_consensus          = File.expand_path(virus_consensus_path)

        @out_dir                  = File.expand_path(out_dir)
        @tmp_dir                  = File.expand_path(tmp_dir) if tmp_dir

        @read_group               = read_group
        @threads                  = threads || 1

        @max_cluster_distance     = max_cluster_distance || 500
        @candidate_found_flag     = true
      end

      def before_run
        @samtools = Tools::Samtools.new(threads: @threads, tmp_dir: @tmp_dir)
        @bwa      = Tools::BWA.new(read_group: @read_group)
        @bedtools = Tools::Bedtools.new
        mkdir_p @out_dir
      end

      def after_run
        check_output_files
        show_results
        @candidate_found_flag
      end

      def main_run
        # Converts the bam file containing the discordant read generated in
        # (1_filter.rb) to fastq file format.

        # shuffle and group alignments by name.
        samtools.collate '-@', @threads,
                         '-o', odir('human_one_end_mapped.human.collate.bam'),
                         @human_one_end_mapped_bam

        # Extract the fastq file from the bam file.
        samtools.fastq '-N',
                       '-@', @threads,
                       '-o', odir('human_one_end_mapped.fastq'),
                       dir('human_one_end_mapped.human.collate.bam')

        # Create BWA Index for HBV consensus reference genome.
        bwa.index @virus_consensus

        # Mapping one end anchored reads to the HBV reference genome.
        bwa.mem '-p',
                '-R', bwa[:RG],
                '-t', @threads,
                '-v', bwa[:verbosity],
                '-o', odir('human_one_end_mapped.virus_consensus.sam'),
                @virus_consensus,
                dir('human_one_end_mapped.fastq')

        # SAM to BAM
        samtools.sort! dir('human_one_end_mapped.virus_consensus.sam')

        # Indexing
        samtools.index! dir('human_one_end_mapped.virus_consensus.sort.bam')

        # Record the names of the mapped reads
        samtools.view '-F 4',
                      dir('human_one_end_mapped.virus_consensus.sort.bam'),
                      '| cut -f1',
                      '| sort -V',
                      '| uniq',
                      '>',
                      odir('human-virus_pair_read_names.txt')

        # Exit if no mapped read found.
        #
        # TODO: The human_unmapped_reads should be mapped to the viral genome
        # to check if there are really no reads to be mapped to virus.
        # When the discordant read cannot be detected, it will be difficult to
        # identify the integration site.
        # But soft-clip may help us to identify some integration sites...
        if File.read(dir('human-virus_pair_read_names.txt')).empty?
          puts 'No candidate pair-end read found.'
          @candidate_found_flag = false
          return
        end

        # Narrow down regions of possible integration.

        # Copying the header to `candidate.sam`.
        samtools.view '-H',
                      '-o', odir('human-virus_pair.human.sam'),
                      @human_one_end_mapped_bam

        # Extract reads from Bam file by read name.
        # picard FilterSamReads is recommended (for better performance).
        # See https://www.biostars.org/p/464226/
        # But we don't use picards here for two reasons.
        # 1. Sometimes picard can fail to parse the @PG tag
        # 2. I like tools that have fewer dependencies.
        samtools.view @human_one_end_mapped_bam,
                      '| grep -F -w -f', dir('human-virus_pair_read_names.txt'), '-',
                      '>>',
                      odir('human-virus_pair.human.sam')

        # Sorting
        samtools.sort! dir('human-virus_pair.human.sam')

        # Convert bam file to bed file
        bedtools.bamtobed '-i', dir('human-virus_pair.human.sort.bam'),
                          '>',  odir('candidate_reads.bed')

        # clustering
        # This is for recording purposes only.
        bedtools.cluster '-i', dir('candidate_reads.bed'),
                         '-d', @max_cluster_distance, # FIXME
                         '>', odir('candidate_regions_clustered_read.bed')

        # merge - combines overlapping features.
        bedtools.merge '-i', dir('candidate_reads.bed'),
                       '-d', @max_cluster_distance,
                       '>', odir('candidate_regions.bed')
      end
    end
  end
end

# frozen_string_literal: true

# VirusPicker: https://github.com/kojix2/viruspicker
# Author: kojix2 <2xijok@gmail.com>

require 'tty-tree'
require 'tty-screen'

module VirusPicker
  module Flows
    # Base class for all flows in VirusPicker
    # The input file must be specified as an absolute path.
    # Each flow has one output directory
    class BaseFlow < Miniflow::Flow
      attr_reader :samtools, :bwa, :minimap2, :bcftools, :bedtools, :delly
    end
  end
end

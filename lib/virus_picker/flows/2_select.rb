# frozen_string_literal: true

# VirusPicker: https://github.com/kojix2/viruspicker
# Author: kojix2 <2xijok@gmail.com>

require_relative 'base_flow'

module VirusPicker
  module Flows
    # Select the best virus sequences.
    class Select < BaseFlow
      # @param human_unmapped_all_bam_path [String]
      #        The path of the bam file containing a potential virus sequence
      # @param virus_sequences_path [String]
      #        A set of reference genomes (fasta) of the viruses
      # @param out_dir [String] Specify the output directory of the files
      # @param tmp_dir [String] Specify where Samtools writes temporary files
      # @param read_group [String]
      #        Read group header line such as '@RG\tID:foo\tSM:bar' used in bwa
      # @param threads [Integer] The number of threads used in Samtools
      def initialize(human_unmapped_all_bam_path,
                     virus_sequences_path,
                     out_dir:,
                     tmp_dir:    nil,
                     read_group: nil,
                     threads:    nil)

        @human_unmapped_bam = File.expand_path(human_unmapped_all_bam_path)
        @virus_sequences    = File.expand_path(virus_sequences_path)

        @out_dir    = File.expand_path(out_dir)
        @tmp_dir    = File.expand_path(tmp_dir) if tmp_dir

        @read_group = read_group
        @threads    = threads || 1
      end

      def before_run
        @bwa        = Tools::BWA.new(read_group: @read_group)
        @samtools   = Tools::Samtools.new(threads: @threads, tmp_dir: @tmp_dir)
        check_file(@human_unmapped_bam, ['.bam'])
        check_file(@virus_sequences, ['.fa', '.fas', '.fasta'])
        mkdir_p @out_dir
      end

      def after_run
        check_output_files
        show_results
      end

      def main_run
        # Shuffle and group alignments by name.
        samtools.collate '-@', @threads,
                         '-o', odir('human_unmapped_all.human.collate.bam'),
                         @human_unmapped_bam

        # Extract the fastq file from the bam file.
        samtools.fastq '-N', # always append /1 and /2 to the read name
                       '-@', @threads,
                       '-o', odir('human_unmapped_all.fastq'),
                       dir('human_unmapped_all.human.collate.bam')

        # Not implemented filtering yet
        #
        # In case of HDT-ID
        # * Mapped to human genome again -> maybe not required?
        # * Remove low complexity reads
        # * Remove low quality reads

        # Make index
        bwa.index @virus_sequences # FIXME: (skip if index alreaddy exists)

        # Mapping unmapped reads to the virus reference sequences.
        bwa.mem '-p', # smart pairing (ignoring in2.fq)
                '-R', bwa[:RG],
                '-t', @threads,
                '-o', odir('human_unmapped_all.viruses.sam'),
                '-v', bwa[:verbosity], # verbosity level
                @virus_sequences,
                dir('human_unmapped_all.fastq')

        # Sorting
        samtools.sort! dir('human_unmapped_all.viruses.sam')

        # Indexing
        samtools.index! dir('human_unmapped_all.viruses.sort.bam')

        # Show flagstat
        samtools.flagstat '-@', @threads,
                          dir('human_unmapped_all.viruses.sort.bam')

        # Here, we choose a sequence based on the number of mapped reads.
        # Another way is to pick the sequence with the highest coverage.
        # The choice of sequence is only an approximation.
        samtools.coverage dir('human_unmapped_all.viruses.sort.bam'), # '-q', '-Q'
                          '| sort -n -k 4', # numreads
                          '| tee', odir('viruses_coverage_count.txt'),
                          '| tail -n1',
                          '| awk \'{print $1}\'',
                          '| tee', odir('selected_virus_name.txt')

        selected_virus_name = File.read(dir('selected_virus_name.txt')).chomp

        # Copy the selected sequence to a new file
        samtools.faidx @virus_sequences,
                       "\"#{selected_virus_name}\"",
                       '-o', odir('selected_virus.fa')

        # Indexing for bwa
        bwa.index dir('selected_virus.fa')

        # Mapping unmapped reads to the selected virus reference genome.
        bwa.mem '-p', # smart pairing
                '-R', bwa[:RG],
                '-t', @threads,
                '-v', bwa[:verbosity],
                '-o', odir('human_unmapped_all.selected_virus.sam'),
                dir('selected_virus.fa'),
                dir('human_unmapped_all.fastq')

        # Sorting
        samtools.sort! dir('human_unmapped_all.selected_virus.sam')

        # Indexing
        samtools.index! dir('human_unmapped_all.selected_virus.sort.bam')

        # Show flagstat
        samtools.flagstat '-@', @threads,
                          dir('human_unmapped_all.selected_virus.sort.bam')
      end
    end
  end
end

# frozen_string_literal: true

# VirusPicker: https://github.com/kojix2/viruspicker
# Author: kojix2 <2xijok@gmail.com>

require_relative 'base_flow'

module VirusPicker
  module Flows
    # Reference genome customization.
    class Host < BaseFlow
      # @param human_reference_path [String] masked.fa
      # @param bed_path [String]
      # @param bam_path [String]
      # @param out_dir [String] Specify the output directory of the files
      # @param tmp_dir [String] Specify where Samtools writes temporary files
      # @param threads [Integer] The number of threads used in Samtools
      def initialize(human_reference_path,
                     bed_path,             # candidate_clusters.bed
                     bam_path,             # should be sorted
                     out_dir:,
                     tmp_dir:   nil,
                     threads:   nil,
                     slop_size: nil)

        @human_reference_path = File.expand_path(human_reference_path)
        @bed_path             = File.expand_path(bed_path)
        @bam_path             = File.expand_path(bam_path)

        @out_dir              = File.expand_path(out_dir)
        @tmp_dir              = File.expand_path(tmp_dir) if tmp_dir

        @threads              = threads   || 1
        @slop_size            = slop_size || 100
      end

      def before_run
        @samtools = Tools::Samtools.new(threads: @threads, tmp_dir: @tmp_dir)
        @bedtools = Tools::Bedtools.new
        @bcftools = Tools::Bcftools.new
        mkdir_p @out_dir
      end

      def after_run
        check_output_files
        show_results
      end

      def main_run
        # Indexing the reference genome.
        # FIXME: Write to the directory where the references are located.
        samtools.faidx @human_reference_path unless File.exist?("#{@human_reference_path}.fai")

        # Create so-called `genome` file used in bedtools.
        # For more information on file formats, see the link below.
        # https://bedtools.readthedocs.io/en/latest/content/general-usage.html#genome-file-format
        cmd.run "awk '{print $1 \"\\t\" $2}' #{@human_reference_path}.fai" \
                " > #{odir('human_bedtools.genome')}"

        # Set a margin for the candidate regions.
        bedtools.slop '-i', @bed_path,
                      '-g', dir('human_bedtools.genome'),
                      '-b', @slop_size,
                      '>', odir('candidates_regions_extended.bed')

        # Find the chromosomes that contain candidate regions.
        # This is to reduce the amount of calculation.
        target_chr = cmd.run2('cat', dir('candidates_regions_extended.bed'),
                              '| cut -f1',
                              '| sort',
                              '| uniq').stdout.split("\n")

        # Extract chromosomes that contain candidate regions.
        samtools.faidx @human_reference_path,
                       *target_chr,
                       '-o', odir('human_target_chromosomes.fa')

        # Update the genome file of the extracted reference genome.
        cmd.run2 'grep',
                 *target_chr.map { |c| "-e #{c}" },
                 dir('human_bedtools.genome'),
                 '>', odir('human_target_bedtools.genome')

        # Masked Fasta Strategy
        # masked.fa
        bedtools.complement '-i', dir('candidates_regions_extended.bed'),
                            '-g', dir('human_target_bedtools.genome'),
                            '>', odir('clusters_extended_complement.bed')

        bedtools.maskfasta '-fi', dir('human_target_chromosomes.fa'),
                           '-bed', dir('clusters_extended_complement.bed'),
                           '-fo', odir('human_target_chromosomes.masked.fa') # FIXME

        # Extract the reads mapped to the candidate regions.
        samtools.view '-L', @bed_path,
                      '-@', @threads,
                      '-o', odir('human_target_regions.human.bam'),
                      @bam_path

        # Variant calling using Bcftools
        # Based on http://kazumaxneo.hatenablog.com/entry/2019/08/03/204318

        # variant call
        # mpileup: multi-way pileup producing genotype likelihoods
        bcftools.mpileup '--max-depth', 500,      # memory usage
                         '-f',          dir('human_target_chromosomes.masked.fa'),
                         '--threads',   @threads, # used only for the compression
                         '-O',          'b',      # compressed BCF
                         '-o',          odir('human_target_regions_genotype.vcf'),
                         dir('human_target_regions.human.bam')

        # SNP/indel variant calling from VCF/BCF
        bcftools.call    '-vm',                  # variants-only, multiallelic-caller
                         '--ploidy',  1,         # Treat all samples as haploid
                         '--threads', @threads,  # used only for the compression
                         '-O',        'z',       # compressed VCF
                         '-o',        odir('human_target_regions.vcf.gz'),
                         dir('human_target_regions_genotype.vcf')

        # Indexing
        bcftools.index dir('human_target_regions.vcf.gz')

        # Left-align and normalize indels
        bcftools.norm '-f',        dir('human_target_chromosomes.masked.fa'),
                      '--threads', @threads,     # used only for the compression
                      '-O',        'b',          # compressed BCF
                      '-o',        odir('human_target_regions.norm.bcf'),
                      dir('human_target_regions.vcf.gz')

        # filter adjustment indels within 5bp
        #
        # https://www.biostars.org/p/99940/
        # Indel Gap, in context of filtering refers to minimum distance between an Indel and a SNP.
        # Indels can cause mapping artifacts and may generate false positive SNPs nearby.
        # Thus, SNPs that lie in the vicinity of an indel are filtered. I use 20 bp threshold.
        # So in my filtered VCF file, there will be no SNPs within 20 bp of Indel.

        bcftools.filter '--IndelGap', 5,
                        '--threads',  @threads,  # used only for the compression
                        '-O',         'z',       # compressed VCF
                        '-o',         odir('human_target_regions.norm.fit-indels.vcf.gz'),
                        dir('human_target_regions.norm.bcf')

        # Indexing
        bcftools.index '--threads', @threads, # used only for the compression
                       dir('human_target_regions.norm.fit-indels.vcf.gz')

        # Create consensus sequence by applying VCF variants to a reference fasta file.
        bcftools.consensus '-f', dir('human_target_chromosomes.masked.fa'),
                           '-c', odir('host_to_consensus.chain'), # write a chain file for liftover
                           '-o', odir('host_consensus.fa'),
                           dir('human_target_regions.norm.fit-indels.vcf.gz')
      end
    end
  end
end

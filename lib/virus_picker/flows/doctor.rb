# frozen_string_literal: true

# VirusPicker: https://github.com/kojix2/viruspicker
# Author: kojix2 <2xijok@gmail.com>

require_relative 'base_flow'

module VirusPicker
  module Flows
    # Check the availability of the command line tools.
    class Doctor < BaseFlow
      def initialize(*)
        @error = false
      end

      def main_run
        Tools.constants.each do |const|
          tool = Tools.const_get(const)
          next unless tool.superclass == VirusPicker::Tools::BaseTool

          begin
            tool.new
          rescue StandardError => e
            @error = true
            puts e.message
          end
        end
      end

      def error?
        @error
      end
    end
  end
end

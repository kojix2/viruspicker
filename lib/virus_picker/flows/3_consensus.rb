# frozen_string_literal: true

# VirusPicker: https://github.com/kojix2/viruspicker
# Author: kojix2 <2xijok@gmail.com>

require_relative 'base_flow'

module VirusPicker
  module Flows
    # Make a consensus sequence for the virus based on the BAM file.
    class Consensus < BaseFlow
      # @param human_unmapped_all_virus_bam_path [String]
      #   Path of the BAM file
      # @param selected_seq [String] Path of selected virus sequence (fasta)
      # @param out_dir [String] Specify the output directory of the files
      # @param threads [Integer] The number of threads used in Samtools
      def initialize(human_unmapped_all_virus_bam_path,
                     selected_virus_sequence_path,
                     out_dir:,
                     threads: nil)

        @virus_bam      = File.expand_path(human_unmapped_all_virus_bam_path)
        @virus_sequence = File.expand_path(selected_virus_sequence_path)

        @out_dir = File.expand_path(out_dir)

        @threads = threads || 1
      end

      def before_run
        @bcftools = Tools::Bcftools.new
        mkdir_p @out_dir
      end

      def after_run
        check_output_files
        show_results
      end

      def main_run
        # Variant calling using Bcftools
        # Based on http://kazumaxneo.hatenablog.com/entry/2019/08/03/204318

        # variant call
        # mpileup: multi-way pileup producing genotype likelihoods
        bcftools.mpileup '--max-depth', 500,               # memory usage
                         '-f',          @virus_sequence,   # reference sequence
                         '--threads',   @threads,          # used only for compression
                         '-O',          'b',               # compressed BCF
                         '-o',          odir('virus_genotypes.vcf'),
                         @virus_bam

        # SNP/indel variant calling from VCF
        bcftools.call '-vm',                     # variants-only, multiallelic-caller
                      '--ploidy',  1,            # Treat all samples as haploid
                      '--threads', @threads,     # used only for the compression
                      '-O',        'z',          # compressed VCF
                      '-o',        odir('virus.vcf.gz'),
                      dir('virus_genotypes.vcf')

        # Indexing
        bcftools.index '--threads', @threads, # used only for the compression
                       dir('virus.vcf.gz')

        # Left-align and normalize indels
        bcftools.norm '-f',        @virus_sequence, # reference sequence
                      '--threads', @threads,        # used only for the compression
                      '-O',        'b',             # compressed BCF
                      '-o',        odir('virus.norm.bcf'),
                      dir('virus.vcf.gz')

        # filter adjustment indels within 5bp
        #
        # https://www.biostars.org/p/99940/
        # Indel Gap, in context of filtering refers to minimum distance between an Indel and a SNP.
        # Indels can cause mapping artifacts and may generate false positive SNPs nearby.
        # Thus, SNPs that lie in the vicinity of an indel are filtered. I use 20 bp threshold.
        # So in my filtered VCF file, there will be no SNPs within 20 bp of Indel.

        bcftools.filter '--IndelGap', 5,
                        '--threads',  @threads,    # used only for the compression
                        '-O',         'z',         # compressed VCF
                        '-o',         odir('virus.norm.fit-indels.vcf.gz'),
                        dir('virus.norm.bcf')

        # Indexing
        bcftools.index '--threads', @threads, # used only for the compression
                       dir('virus.norm.fit-indels.vcf.gz')

        # Create consensus sequence by applying VCF variants to a reference fasta file.
        bcftools.consensus '-f', @virus_sequence, # reference sequence
                           '-c', odir('virus_to_consensus.chain'), # write a chain file for liftover
                           '-o', odir('virus_consensus.fa'),
                           dir('virus.norm.fit-indels.vcf.gz')
      end
    end
  end
end

# frozen_string_literal: true

# VirusPicker: https://github.com/kojix2/viruspicker
# Author: kojix2 <2xijok@gmail.com>

require 'miniflow'

require_relative 'virus_picker/version'
require_relative 'virus_picker/tools'
require_relative 'virus_picker/flows'
require_relative 'virus_picker/command'

# Virus Picker is a tool to detect virus integration site.
module VirusPicker
  TTYCMD = Miniflow::TTYCMD
end

# Virus Picker

![test](https://github.com/kojix2/viruspicker/workflows/test/badge.svg)

:microscope: Simple scripts to find HBV integration sites.

## Installation

Install dependencies and add them to the `PATH`.

- [Ruby](https://github.com/ruby/ruby)
  - [miniflow](https://github.com/kojix2/miniflow)
- [Samtools](https://github.com/samtools/samtools)
- [Bcftools](https://github.com/samtools/bcftools)
- [BWA](https://github.com/lh3/bwa)
- [bedtools](https://github.com/arq5x/bedtools2)
- [Delly](https://github.com/dellytools/delly)

Then install viruspicker with ruby gem package manager.

```sh
gem install viruspicker
```

If you want to install it from source code:

```sh
git clone https://github.com/kojix2/viruspicker
cd viruspicker
rake install
```

## Usage

Basic usage:

```sh
viruspicker call sample.bam human.ref.fa viral-seqs.fa
```

Show help:

```sh
viruspicker --help
```

```
Program: viruspicker
Version: 0.0.1.beta
Contact: kojix2 <2xijok@gmail.com>

Usage:   viruspicker <command> [options]

Command:
    call         run all
    doctor       check the dependent tools are available

 -- sub processes
    filter       1. extract unmapped reads from a bam file
    select       2. select the closest viral sequence
    consensus    3. make the consensus sequence of virus
    predetect    4. predict the integration sites with pair end reads
    host         5. make the consensus sequence of host.
    detect       6. detect virus integration sites more accurately
```

You can use the `viruspicker doctor` command to check if the dependent tools are availabile and on the path.

```sh
viruspicker doctor
```

## HBV reference genome

- [HBVdb](https://hbvdb.lyon.inserm.fr/HBVdb/)

Use [scripts/download_hbv.sh](https://github.com/kojix2/viruspicker/tree/main/scripts) to download HBV reference genomes.

## Human release genome

- [GENCODE - Human](https://www.gencodegenes.org/human/)
- [Telomere-to-telomere](https://github.com/marbl/CHM13)
- [JG2](https://jmorp.megabank.tohoku.ac.jp/202102/downloads/)

Download and place the file manually from the above sites or others.

## Docker

Build:

```sh
git clone https://github.com/kojix2/viruspicker
cd viruspicker
sudo docker build -t kojix2/viruspicker .
```

Run:

```sh
sudo docker run -it kojix2/viruspicker
# sudo docker run -it -v $(pwd):/tmp/share kojix2/viruspicker
```

## License

MIT

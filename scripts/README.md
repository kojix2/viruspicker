# download_hbv.sh

* This shell script downloads the hepatitis B genome from [HBVdb](https://hbvdb.lyon.inserm.fr/HBVdb/). 
* It first downloads Genotypes A, B, and C.
  * A_Genomes.fas
  * B_Genomes.fas
  * C_Genomes.fas
* Next, integrate these files into Genomes.fas.
  * Genomes.fas

This file can be used as input for the `viruspicker select` command.
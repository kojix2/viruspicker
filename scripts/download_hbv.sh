#!/usr/bin/env bash
# Summary: Download HBV reference sequences.
# Author: kojix2 <2xijok@gmail.com>

set -eu

cd $(dirname $0)

if [ -e A_Genomes.fas ]; then
  echo "A_Genomes.fas found."
else
  curl -O https://hbvdb.lyon.inserm.fr/data/nucleic/fasta/A_Genomes.fas
fi

if [ -e B_Genomes.fas ]; then
  echo "B_Genomes.fas found."
else
  curl -O https://hbvdb.lyon.inserm.fr/data/nucleic/fasta/B_Genomes.fas
fi

if [ -e C_Genomes.fas ]; then
  echo "C_Genomes.fas found."
else
  curl -O https://hbvdb.lyon.inserm.fr/data/nucleic/fasta/C_Genomes.fas
fi

if [ -e Genomes.fas ]; then
  echo "Genomes.fas found."
else
  cat A_Genomes.fas B_Genomes.fas C_Genomes.fas >Genomes.fas
fi

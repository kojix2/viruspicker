library(karyoploteR, warn.conflicts=FALSE, quietly=TRUE)

file_path <- commandArgs(trailingOnly=TRUE)[1]
save_dir_path <- commandArgs(trailingOnly=TRUE)[2]
title_name <- commandArgs(trailingOnly=TRUE)[3]

pdf(file=file.path(save_dir_path, "one_end_mapped.pdf"), width=20, height=10)
kp <- plotKaryotype(genome="hg38", plot.type = 4, main=title_name)
kp <- kpPlotBAMDensity(kp, data = file_path, col="red")
dev.off()
# Miniconda3
FROM continuumio/miniconda3

LABEL maintainer="2xijok@gmail.com"

# Install required packages
RUN apt-get update && \
    apt-get install -y \
    ps \
    git \
    autoconf \
    bison \
    build-essential \
    libssl-dev \
    libyaml-dev \
    libreadline6-dev \
    zlib1g-dev \
    libncurses5-dev \
    libffi-dev \
    libgdbm-dev \
    libdb-dev \
    libboost-date-time-dev \
    libboost-program-options-dev \
    libboost-system-dev \
    libboost-filesystem-dev \
    libboost-iostreams-dev \
    && apt-get clean
    
# Install Ruby
RUN git clone --depth=1 https://github.com/rbenv/ruby-build && \
    PREFIX=/usr/local ./ruby-build/install.sh && \
    rm -rf ruby-build && \
    ruby-build 3.0.2 /usr/local

# Install dependencies with bioconda
RUN conda config --add channels bioconda && \
    conda update -n base conda && \
    conda install samtools && \
    conda install bwa && \
    conda install minimap2 && \
    conda install bcftools && \
    conda install bedtools && \
    conda install delly

# Install viruspicker
COPY ./ /root/viruspicker
RUN cd /root/viruspicker && \
    rake install

# Workdir
WORKDIR /root/viruspicker

# by default /bin/sh is executed
CMD ["/bin/bash"]
